Screenshots:


![Server-side validation turned off](images/screenshot2.png)


![Displays user-entered data, as well as the ability to modify/delete the data](images/screenshot3.png)


[Link to local lis4381 web app](https://localhost/repos/4381)