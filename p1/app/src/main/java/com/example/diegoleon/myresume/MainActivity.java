package com.example.diegoleon.myresume;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = findViewById(R.id.buttonDetails);
        button.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick (View V) {
            startActivity(new Intent(MainActivity.this, resumeDetails.class));

        }

            });
}}



