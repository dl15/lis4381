Git commands


1. git init: create an empty Git repository or reinitialize an existing one 

2. git status: show the working tree status

3. git add: add file contents to index

4. git commit: record changes to the repository

5. git push: update remote refs along with associated objects

6. git pull: fetch from and integrate with another repository or a local branch

7. git stash: stash the changes in a dirty working directory away


Screenshot of AMPPS: 
![](images/AMPPS.png)


Screenshot of the emulator: 
![](images/emulator.png)

Screenshot of the java sdk version 
![](images/java-sdk-version.png)

[Link to BitbucketStationLocations](https://bitbucket.org/dl15/bitbucketstationlocations/src/master/)