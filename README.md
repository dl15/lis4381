# LIS4381 - Mobile Web Apps

## Diego Leon

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md)

    * Install AMPPS

    * Install JDK

    * Install Android Studio and Create My First App

    * provide screenshots of installations

    * Create Bitbucket Repo

    * Complete Bitbucket tutorial (bitbucketstationlocations)

    * Provide git command descriptions

2. [A2 README.md](a2/README.md)

    * Create Healthy Recipes Android app

    * Provide screenshots of completed app

3. [A3 README.md](a3/README.md)

    * Create ERD based upon business rules

    * Provide screenshot of completed ERD

    * Provide DB resource links

4. [A4 README.md](a4/README.md)

    * Add jQuery validation and regular expressions

    * Create a favicon with *your* initials

    * Include screenshots in README.md 

5. [A5 README.md](a5/README.md)

    * Add server-side validation

    * Include JQuery validation to all the fields

    * Include screenshots in README.md 

6. [P1 README.md](p1/README.md)

    * Create a launcher icon image   
    
    * Add a background to both activities  

    * Add border around the images and button

    * Add text shadow to the button

    * Screenshot both activity screens

7. [P2 README.md](p2/README.md)

    * Adds functionality to A5 assignment

    * Complete the edit and delete functions

    * Turn off client-side validation to test out server-side 